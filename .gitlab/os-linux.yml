# Linux-specific builder configurations and build commands

## Base images

.linux:
    variables:
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
        GIT_SUBMODULE_STRATEGY: none

.ubuntu18:
    extends: .linux
    image: "kitware/vtk:fides-ci-ubuntu18-ompi-20240709"

.ubuntu_compiler_gcc_mixin:
    variables:
        CC: gcc
        CXX: g++

.ubuntu_compiler_clang_mixin:
    variables:
        CC: clang
        CXX: clang++

.ubuntu18_gcc: &ubuntu18_gcc
    extends:
        - .ubuntu18
        - .ubuntu_compiler_gcc_mixin
    variables:
        CMAKE_CONFIGURATION: ubuntu18_gcc

.ubuntu18_clang: &ubuntu18_clang
    extends:
        - .ubuntu18
        - .ubuntu_compiler_clang_mixin
    variables:
        CMAKE_CONFIGURATION: ubuntu18_clang

.ubuntu18_tidy: &ubuntu18_tidy
    extends:
        - .ubuntu18
        - .ubuntu_compiler_clang_mixin

    variables:
        CMAKE_CONFIGURATION: ubuntu18_tidy
        CTEST_NO_WARNINGS_ALLOWED: 1

.ubuntu18_memcheck: &ubuntu18_memcheck
    extends:
        - .ubuntu18
        - .ubuntu_compiler_gcc_mixin
    image: "kitware/vtk:fides-ci-ubuntu18-20240709"

    variables:
        CMAKE_BUILD_TYPE: RelWithDebInfo

.ubuntu18_asan: &ubuntu18_asan
    extends: .ubuntu18_memcheck

    variables:
        CMAKE_CONFIGURATION: ubuntu18_asan
        CTEST_MEMORYCHECK_TYPE: AddressSanitizer

.ubuntu18_ubsan: &ubuntu18_ubsan
    extends: .ubuntu18_memcheck

    variables:
        CMAKE_CONFIGURATION: ubuntu18_ubsan
        CTEST_MEMORYCHECK_TYPE: UndefinedBehaviorSanitizer

.ubuntu18_coverage: &ubuntu18_coverage
    extends:
        - .ubuntu18
        - .ubuntu_compiler_gcc_mixin

    variables:
        CMAKE_BUILD_TYPE: Debug
        CMAKE_CONFIGURATION: ubuntu18_coverage
        CTEST_COVERAGE: 1
        CMAKE_GENERATOR: Unix Makefiles

.linux_builder_tags:
    tags:
        - build
        - docker
        - linux-x86_64
        - vtk

.linux_tester_priv_tags:
    tags:
        - docker
        - linux-x86_64
        - privileged
        - vtk

## Linux-specific scripts

.before_script_linux: &before_script_linux
    - .gitlab/ci/cmake.sh
    - .gitlab/ci/ninja.sh
    - export PATH=$PWD/.gitlab/:$PWD/.gitlab/cmake/bin/:$PATH
    - cmake --version
    - ninja --version
    - export LSAN_OPTIONS="suppressions=$PWD/.gitlab/ci/asan-suppressions.txt"

.cmake_build_linux:
    stage: build
    extends: .warning_policy

    script:
        - *before_script_linux
        - .gitlab/ci/sccache.sh
        - export PATH=$PWD/.gitlab:$PATH
        - sccache --start-server
        - sccache --show-stats
        - $LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake
        - $LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake
        - sccache --show-stats
        - exec .gitlab/ci/check_warnings.sh .

    interruptible: true

.cmake_test_linux:
    stage: test

    script:
        - *before_script_linux
        - ctest -V --output-on-failure -S .gitlab/ci/ctest_test.cmake

    interruptible: true

.cmake_memcheck_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_memcheck.cmake"
    interruptible: true

.cmake_coverage_linux:
    stage: analyze

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_coverage.cmake"
    coverage: '/Percentage Coverage: \d+.\d+%/'
    interruptible: true
