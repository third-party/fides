##  Support reading vectors of any size

FIDES allows you to treat one of the dimensions of an array as a vector.
Previously, FIDES supported only vectors of size 1, 2, and 3. FIDES can
now read vectors of any size.
