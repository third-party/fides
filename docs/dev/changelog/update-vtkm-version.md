## Update the minimum VTK-m to 2.1

VTK-m 2.1 contains some important features to support reading and writing
vector fields of different sizes. FIDES is updated to require this newer
version to take advantage of these features.
