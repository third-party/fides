## Building ParaView/VTK with Fides Reader

ParaView and VTK support using Fides as a reader for bp files.
ParaView should be built with CMake option `-DPARAVIEW_ENABLE_FIDES=ON`.
Fides is included as a third party repo in VTK, so you do not need to point CMake to your own Fides build.

## Running Fides XGC Python script
The `xgc.py` script in this directory shows how you can use Fides with the ParaView Simple Python module for visualization tasks for XGC data.
To run, you can do the following:
```
$ /path/to/paraview-build/bin/pvbatch ./xgc.py \
  --fides_attributes=/path/to/xgc-attr.bp \
  --output_path=/path/for/output \
  --varname=dpot
```

The xgc-attr.bp is a simple file containing some attributes that gives Fides the info it needs to correctly read the data.
Fides expects this file to be located in the same directory where the XGC data is saved.
You point the script to this file and Fides will be able to find the data.
There is a python script located at `scripts/xgc-attributes.py` that can be used for creating this file, or you can use the one currently saved in `tests/data`.

For the command line options used by the example script:
  - fides_attributes: path to the attributes file described above
  - output_path: path to directory where rendered images should be saved
  - varname: Variable to visualize, `dpot` variable only currently

## Using EFFIS to run Python script with XGC
This example has also been tested running with [EFFIS](https://github.com/suchyta1/kittie).
The sample yaml file can be used for setting this up (although paths will need to be updated appropriately).

To set up you call the following EFFIS/Kittie commands:
```
$ kittie-compose.py xgc-cyclone-paraview.yaml
$ kittie-submit /path/to/jobs/xgc-paraview
```

The `jobs/xgc-paraview` directory is the `rundir` set in the yaml file.
