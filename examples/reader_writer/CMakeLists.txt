set(VTKM_LIBS vtkm::filter)
if (VTKm_ENABLE_RENDERING)
  set(RENDERING_DEFS USE_VTKM_RENDERING)
  set(RENDERING_LIBS vtkm::rendering)
endif()
add_executable(writer writer.cxx)
vtkm_add_target_information(writer
                            DROP_UNUSED_SYMBOLS
                            MODIFY_CUDA_FLAGS
                            DEVICE_SOURCES writer.cxx)
target_compile_definitions(writer PRIVATE ${MPI_DEFS} ${RENDERING_DEFS})
target_link_libraries(writer fides adios2::adios2 ${VTKM_LIBS} ${RENDERING_LIBS} ${MPI_LIBS})

add_executable(reader reader.cxx)
vtkm_add_target_information(reader
                            DROP_UNUSED_SYMBOLS
                            MODIFY_CUDA_FLAGS
                            DEVICE_SOURCES reader.cxx)
target_compile_definitions(reader PRIVATE ${MPI_DEFS} ${RENDERING_DEFS})
target_link_libraries(reader fides adios2::adios2 ${VTKM_LIBS} ${RENDERING_LIBS} ${MPI_LIBS})
